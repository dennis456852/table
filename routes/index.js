var express = require('express');
var router = express.Router();
const fs = require('fs')
const path = require('path')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(200).send(path.resolve('..', 'public', 'index.html'));
});

router.get('/nodes', function (req, res) {
  fs.readFile(path.resolve(__dirname, '..', 'data', 'nodes.json'), function (err, data) {
    if (err) res.status(500).send(`讀取列表失敗`)
    else {
      res.status(200).send(data.toString())
    }
  })
})

router.post('/node', function (req, res) {
  const { id, ip, coinbase } = req.body
  fs.readFile(path.resolve(__dirname, '..', 'data', 'nodes.json'), function (err, data) {
    if (err) res.status(500).send(`讀取列表失敗`)
    else {
      let nodes = JSON.parse(data.toString())

      nodes.push({ id: id, ip: ip, coinbase: coinbase })

      fs.writeFile(path.resolve(__dirname, '..', 'data', 'nodes.json'), JSON.stringify(nodes), function (err) {
        if (err) res.status(500).send(`新增失敗`)
        else {
          res.status(200).send(`新增成功`)
        }
      })
    }
  })
})

router.delete('/node', function (req, res) {
  const { id, ip, coinbase } = req.body
  fs.readFile(path.resolve(__dirname, '..', 'data', 'nodes.json'), function (err, data) {
    if (err) res.status(500).send(`讀取列表失敗`)
    else {
      let nodes = JSON.parse(data.toString())

      let index = nodes.findIndex((element) => {return (element.id == id && element.id == ip && element.coinbase == coinbase)})
      nodes.splice(index, 1)

      fs.writeFile(path.resolve(__dirname, '..', 'data', 'nodes.json'), JSON.stringify(nodes), function (err) {
        if (err) res.status(500).send(`刪除失敗`)
        else {
          res.status(200).send(`刪除成功`)
        }
      })
    }
  })
})

module.exports = router;
