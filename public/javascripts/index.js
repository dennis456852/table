$(document).ready(function () {
    $.get("/nodes", function (data, textStatus) {
        let nodes = JSON.parse(data)
        for (let i = 0; i < nodes.length; i++) {
            $('#tb').append(`<tr>
            <td scope="row">${nodes[i].id}</td>
            <td>${nodes[i].ip}</td>
            <td>${nodes[i].coinbase}</td>
            <td>
            <button type="button" class="close" id="delete${i}" value="${nodes[i].id}:${nodes[i].ip}:${nodes[i].coinbase}">
            <span>&times;</span>
          </button>
            </td>
          </tr>`);
          $(`#delete${i}`).click(function (e) {
              e.preventDefault();
              let args = this.value.split(":")
              let data = {
                  id: args[0],
                  ip: args[1],
                  coinbase: args[2]
              }
              $.ajax({
                  type: "delete",
                  url: "/node",
                  data: data,
                  success: function (response) {
                      location.reload()
                  }
              });
          });
        }
    })

    $('#submit').click(function (e) {
        e.preventDefault();
        let data = {
            id: $('#id').val(),
            ip: $('#ip').val(),
            coinbase: $('#coinbase').val(),
        }
        $.post("/node", data,
            function (data, textStatus) {
                location.reload()
            }
        );
    });

})